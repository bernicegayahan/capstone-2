const express = require('express')
const router = express.Router()


const CourseController = require('../controllers/course')

//route to check if course already exists
router.post('course-exists', (req, res) => {
    console.log(CourseController);
})


//let's create our first endpoint that will allow us to send a request to "create" a new course inside the database
//we need to assign a designated endpoint for our request
router.post('/create', (req, res) => {
    //will describe the next set of procedures that will happen upon sending the request in the specified endpoint.
    //upon sending the request to the endpoint you are already instantiating a promise
    CourseController.insert(req.body).then(result => res.send(result));
})

router.post('/course-exists', (req, res) => {
    //will describe the next set of procedures that will happen upon sending the request in the specified endpoint.
    //upon sending the request to the endpoint you are already instantiating a promise
    CourseController.findCourse(req.body).then(result => res.send(result));
})
//lets assign a designated endpoint in retrieveing all records from the course collection
router.get('/', (req, res) => {
    CourseController.getAll().then(courses => res.send(courses));
})

//lets assign a designated endpoint for the get course method
router.get('/:id', (req, res) => {
    let courseId = req.params.id
    CourseController.get({ courseId }).then(course => res.send(course))
})

module.exports = router;
