const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Course name is required.']
    },
    description: {
        type: String,
        required: [true, 'Description is required.']
    },
    price: {
        type: String,
        required: [true, 'Price is required.']
    },
    isActive: { //this key will describe if the course is available for enrollment.
        type: Boolean,
        default: true,
    },
    createdOn: {//will describe the data when the course was added
        type: Date,
        default: new Date(),
    },
    enrollees: [//this will describe/display the list of students enrolled for a specific subject
        {
            userId: { //this will describe the id of the user to get the detauls 
                type: String,
                required: [true, 'User ID is required.']
            },
            enrolledOn: {//this will describe when the stduent enrolled for a specific subject
                type: Date,
                default: new Date()
            }
        }
    ]
})

module.exports = mongoose.model('course', courseSchema)

