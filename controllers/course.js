const Course = require('../models/course')



//1.create a function that will allow us to create a new course inside our database.
//
module.exports.insert = (params) => {
    let course = new Course({
        name: params.name,
        description: params.description,
        price: params.price
    })
    //once you have successfully captured the data inserted by the user, its now time to save it inside the database
    //by applying the save() in the course variable, we are already creating a promise object.
    return course.save().then((course, err) => {
        return (err) ? false : true
    })
} //lets assign an api endpoint for this function

//lets create a new function that will allow us to retrieve all courses inside the database
module.exports.getAll = () => {
    //upon doing this query inside our collection we are instantiating a promise
    return Course.find({ isActive: true }).then(courses => courses)
}

//lets create a new function that will allow us to get course details 
module.exports.get = (params) => {
    //we need to run a query on the course collection to identify which course to display.	
    return Course.findById(params.courseId).then(course => course)
}

//[Secondary Section] // Stretch goals

module.exports.findCourse = (params) => {
    // console.log(params)
    return Course.find({ name: params.name }).then(result => {
        return result.length > 0 ? true : false
    })
}

