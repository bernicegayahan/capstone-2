const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Course = require('../models/course');


module.exports.emailExists = (params) => {
    return User.find({ email: params.email }).then(result => {
        return result.length > 0 ? true : false
    })
}

module.exports.register = (params) => {
    let user = new User({
        firstName: params.firstName,
        lastName: params.lastName,
        email: params.email,
        mobileNo: params.mobileNo,
        password: bcrypt.hashSync(params.password, 10)
    })

    return user.save().then((user, err) => {
        return (err) ? false : true
    })
}

module.exports.login = (params) => {
    return User.findOne({ email: params.email }).then(user => {
        if (user === null) { return false }


        const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

        if (isPasswordMatched) {
            return { accessToken: auth.createAccessToken(user.toObject()) }
        }
        else {
            return false
        }
    })
}

module.exports.get = (params) => {
    return User.findById(params.userId).then(user => {
        user.password = undefined
        return user
    })
}

module.exports.enroll = (params) => {
    console.log("params.body.courseId: " + params.courseId)
    console.log("params.body.userId: " + params.body.userId)
    return Course.updateOne({ _id: params.body.courseId, 'enrollees.userId': { $ne: params.body.userId } },
        { $push: { enrollees: { userId: params.body.userId, enrolledOn: Date.now() } } })
        .then(result => {
            if (result.nModified > 0) {
                return User.updateOne({ _id: params.body.userId, 'enrollments.courseId': { $ne: params.body.courseId } },
                    { $push: { enrollments: { courseId: params.body.courseId, enrolledOn: Date.now() } } })
                    .then(result => { return (result.nModified > 0) ? { status: 200, message: "Course enrolled successfully" } : { status: 400, message: "Something went wrong. Please try again" }; });
            } else {
                return { status: 400, message: "You have already taken this course" };
            }
        })
}

