const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/user');
//lets acquire the route component for course
const courseRoutes = require('./routes/course');

app.use(cors());

require('dotenv').config()

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const portNumber = process.env.PORT;
const connectionString = process.env.DB_CONNECTION_STRING;

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));
mongoose.connect(connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

app.get('/', function (req, res) {
    res.send(`Capstone Server hosted properly online`)
})

app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);

app.listen(portNumber || 4000, () => {
    console.log(`API is now online on port ${portNumber || 4000}`);
});
